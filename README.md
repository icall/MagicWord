[magicword](http://wiki.lezinter.net/index.php/Projets:Magic_Word)
=========

A Symfony3 project implemented mostly ([see all contributors](application/CONTRIBUTORS.md)) by
[Arnaud Bey](http://litt-arts.univ-grenoble-alpes.fr/fr/membres/m-arnaud-bey--4727.kjsp)
and more recently [Sylvain Hatier](https://www.researchgate.net/profile/Sylvain_Hatier) based on :
* https://github.com/InnovaLangues/Magic-Word-game : the first prototype, augmented little by little by various persons from the initial code
of Pauline Rebourgeon and Christine Lutian ;
* https://github.com/giacomo-mambelli/magicword2 : the "Bologne" prototype, coded by Giacomo Mambelli, contains the infamous "Bologna algorithm" cf.
[Associated research](#Associated research) that allows the [conquer mode](https://github.com/InnovaLangues/MagicWord/wiki/Conquer).

See [the wiki](https://gricad-gitlab.univ-grenoble-alpes.fr/lzbk/MagicWord/wikis/home) for more information
(you can also have a look at our
[French teaser](https://vimeo.com/216809745) (english subtitles available)).

### Sounds
All sounds come from [freesound.org](http://www.freesound.org/) and are licensed under
[CC BY 3.0](https://creativecommons.org/licenses/by/3.0/). They've been converted to mp3 when needed :
* http://www.freesound.org/people/chripei/sounds/165492/
* http://www.freesound.org/people/sandyrb/sounds/41381/
* http://www.freesound.org/people/zgump/sounds/82523/
* http://www.freesound.org/people/FOSSA11/sounds/49306/
* http://www.freesound.org/people/rhodesmas/sounds/320655/
* http://www.freesound.org/people/Autistic%20Lucario/sounds/142608/
* http://www.freesound.org/people/cabled_mess/sounds/360602/
* http://www.freesound.org/people/Taira%20Komori/sounds/212527/

### Dictionaries
* French - [Morphalou3](https://repository.ortolang.fr/api/content/morphalou/2/LISEZ_MOI.html#idp37270384) (LGPL-LR)
* English - [DELA](http://infolingu.univ-mlv.fr/DonneesLinguistiques/Dictionnaires/telechargement.html) (LGPL-LR)

Associated research
-------------------
Magic Word is part of the research carried out at the [LIDILEM lab](http://lidilem.u-grenoble3.fr) :
* [10.4000/alsic.2828](http://dx.doi.org/10.4000/alsic.2828) : 1st paper, the ideas behind the game
* [10.1109/ICCNC.2016.7440546](http://sci-hub.cc/10.1109/ICCNC.2016.7440546) : the "Bologna algorithm"
* [10.14705/rpnet.2016.eurocall2016.575](https://halshs.archives-ouvertes.fr/halshs-01422328) : en route to this new version
* this game integrated in a global serious game development strategy :
  * [10.4000/apliut.5742](http://dx.doi.org/10.4000/apliut.5742)
  * [alsic.revues.org/3037](https://alsic.revues.org/3037)

Context and Funding
---------------------
The original idea behind Magic Word, even though probably not that original (teachers have used
[letter games in the language class for a while](https://journals.openedition.org/dhfles/4419)), 
was publicly discussed for the first time on [november 7th 12](http://innovalangues.fr/reflexions-autour-du-jeu/).
Magic Word was one of the driving ideas behind the 
creation of the GAMER workpackage inside the Innovalangues
[IDEFI](http://www.agence-nationale-recherche.fr/investissements-d-avenir/appels-a-projets/2011/initiatives-dexcellence-en-formations-innovantes-idefi/)
project (**ANR-IDFI-0024**).

Magic Word's first versions was designed and implemented within the [GAMER WP](http://innovalangues.fr/realisations/ressources-ludiques/).

Its creation mostly comes from the goodwil and dedication of the GAMER members ([interns or otherwise](application/CONTRIBUTORS.md)) and others afterwards.

The present repository constitutes a **[fork](https://fr.wikipedia.org/wiki/Fork_(développement_logiciel))** of the [work that **we** performed within
the GAMER WP and under the responsibility of Innovalangues](https://github.com/InnovaLangues/MagicWord).
The fork was done under the MIT license. We forked the code to start at the state of the [project](http://innovalangues.fr/realisations/prototypes/) *right after the dismisal of the GAMER WP*.
As such, this work is not subject to the contracts between Innovalangues stakeholders and the [ANR](https://anr.fr) (in terms of logo presence, etc.).
All the same, **we acknowledge the financial support of the ANR through the Innovalangues project
during the early stages of Magic Word and wish to thank both the ANR and Innovalangues for the work we have been able to do thanks to their support**.

The said work ends with [the last commit of our collaboration](https://github.com/InnovaLangues/MagicWord/commit/adc332529125dbe614fed96b2bc0eacfc6d6afc1).
All the work done since [then](4167e013d5d6c283ac741da904ab2a3605ad9c0c) has been done autonomously or thanks to the support of [Démarre SHS!](https://demarreshs.hypotheses.org/Magic_Word).

For a detailed history of our resources, see [our official webpage](http://wiki.lezinter.net/index.php/Projets:Magic_Word#Financement).

Install
-------
see [INSTALL.md](INSTALL.md)


Develop / contribute
-------
see [CONTRIBUTE.md](CONTRIBUTE.md)

Contributors
------
see [application/CONTRIBUTORS.md](application/CONTRIBUTORS.md)