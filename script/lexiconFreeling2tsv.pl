#script prenant en entrée les ressources suivantes : 
	#Dans rep /entries, les fichiers avec nom .+\..+ (pattern $nameLexiconFiles="MM.(adj|adv|int|nom|vaux|verb)")
#		Ex : en espagnol MM.(adj|adv|int|nom|vaux|verb)
		#+ le tagset.data qui explique les catégories et traits

	#en sortie -> lexicon.tsv (le lexique sous la forme word\troot\feat1=value,feat2=value...)
use Encode;
use IO::Handle;
STDOUT->autoflush();
use strict;
use utf8;
use Data::Dumper;


################################## VARIABLE LEXIQUE USER ########################################
my $userLanguage="espagnol";
my $userDescription="ce lexique recouvre les mots  ... et les traits";
my $userRelationType="dérivation, racine, etc ... définition par l'user";


################################## VARIABLE LEXIQUE USER  ########################################


##################################DEBUT STRUCTURE DE DONNEES ########################################

	#######CHEMIN RESSOURCES
my $dirRessource="entries-en";
my $fileTagset="tagset.dat";
my $fileLexiconOut="lexicon.tsv";
my $fileSpecOut="spec.txt";
#my $nameLexiconFiles="lefff\.(adj|adv|int|nom|vaux|verb)";
my $nameLexiconFiles="[^\.]+";
my %cat2features;
# $cat2features{$catLabel}{$featureLabel}{$valuesNLabel[0]}=$valuesNLabel[0]
# $cat2features{"N"}{"type"}{"C"}="common";
my %labels2expand;
# $labels2expand{"N"}="noun";

my %cat2CharCoded;#nombre de caractères sur lesquels est codée la cat
# $cat2CharCoded{"N"}="2";



my %cat2featuresOrder;
# $cat2featuresOrder{catLabel}{$cpt}=$featureLabel;
# $cat2featuresOrder{"N"}{1}="type";

my %wordsLexicon;#$wordsLexicon{"abajaban"} = "abajar	cat=V,type=M,mood=I,tense=I,person=3,num=P,gen=0"

##################################FIN STRUCTURE DE DONNEES ########################################

open(LOG,">:encoding(utf8)","loglst.txt");

#Début lecture 

################################## DEBUT LECTURE TAGSET  ############################
#format
#catégorie	mono/polylexical	lemme	acception_termith avec cat	contexte gauche	pivot	contexte droit	source	partie textuelle
#acception_termith avec cat -> aboutir_V_2 ou absence_N
#attention, les polylexicaux doivent etre en fin de fichier
open(TAGSET,"<:encoding(utf8)",$dirRessource."/".$fileTagset);
my $inRules=0;
my $line;
while ( $line = <TAGSET>) {
	if($line=~/^<\/DecompositionRules>/){
		$inRules=0;
	}
	if($inRules){
		if($line=~/^([A-Z]+)\s(\d+)\s(\S+)\s*(.*)/){
			my ($catLabel, $charCat, $catExpand, $features)=($1,$2,$3,$4);
			$labels2expand{"catégorie"}{$catLabel."=".$catExpand}=1;
			$cat2CharCoded{$catLabel}=$charCat;
			print LOG "$catLabel = $catExpand codé sur $charCat et traits :  $features \n";

			my @featuresArray=split(" ",$features);
			my $cpt=0;
			foreach my $featureCase (@featuresArray){
				
				print LOG "FEATURE = $featureCase \n";
				if($featureCase=~/(.+)\/(.+)/){
					my $featureLabel=$1;
					my $featureValuesString=$2;
					$cat2featuresOrder{$catLabel}{$cpt}=$featureLabel;
					my @featureValuesArray=split(";",$featureValuesString);
					foreach my $featureValue (@featureValuesArray){
						my @valuesNLabel=split(":",$featureValue);
						$labels2expand{$featureLabel}{$valuesNLabel[0]."=".$valuesNLabel[1]}=1;
						print LOG "$featureLabel = ".$valuesNLabel[0]." expand by  ".$valuesNLabel[1]." \n";
						$cat2features{$catLabel}{$featureLabel}{$valuesNLabel[0]}=$valuesNLabel[0];
					}
					
				}
				else{
					print LOG "BLEME RULES feature $featureCase \n";
				}
				$cpt++;

			}
		}
		else{
			print LOG "BLEME RULES EX $line \n";
		}
	}
	if($line=~/^<DecompositionRules>/){
		$inRules=1;
	}
	
	
}
close(TAGSET);
################################## FIN LECTURE TAGSET ############################


################################## DEBUT LECTURE LEXIQUE ENTRIES   #######################################
opendir(LEXICONS,$dirRessource);
while(my $fileLexicon = readdir(LEXICONS)){
	if($fileLexicon=~/^$nameLexiconFiles$/){
		open(LEXICON,"<:encoding(utf8)",$dirRessource."/".$fileLexicon);
		print LOG "LECTURE $fileLexicon \n";
		while ($line = <LEXICON>) {
			if($line=~/^([^\s]+)\s([^\s]+)\s([^\s]+)/){
				my ($word, $root, $features)=($1,$2,$3);
				my @featuresLetter=split("",$features);
				my $cat = $featuresLetter[0];
				my $featuresString ="cat=".$cat.",";
				#on récupère les features dans l'ordre donné par $cat2featuresOrder{$catLabel}
				for my $i (0 .. $#featuresLetter-1) {
					$featuresString .=$cat2featuresOrder{$cat}{$i}."=".$featuresLetter[$i+1].",";
				}
				chop($featuresString);
				$wordsLexicon{$word}=$root."\t".$featuresString;
			}
			else{
				print LOG "BLEME LECTURE LINE $line \n";
			}
		}
		close(LEXICON);
	}
}
closedir(LEXICONS);

################################## FIN LECTURE LEXIQUE ENTRIES  #######################################


################################## DEBUT ECRITURE LEXIQUE TSV   #######################################

open(LEXICONTSV,">:encoding(utf8)",$dirRessource."/".$fileLexiconOut);

my @keysWords=sort { $a cmp $b } keys(%wordsLexicon);
foreach my $keyWords (@keysWords){
	print LEXICONTSV $keyWords."\t".$wordsLexicon{$keyWords}."\n";
}


close(LEXICONTSV);
################################## FIN ECRITURE LEXIQUE TSV #######################################


################################## DEBUT ECRITURE SPEC TXT   #######################################

# DESCR:value(cat):A=adjectif

open(LEXICONTXT,">:encoding(utf8)",$dirRessource."/".$fileSpecOut);

@keysWords=sort { $a cmp $b } keys(%wordsLexicon);

print LEXICONTXT "LANGUAGE=".$userLanguage."\n";
print LEXICONTXT "RELATIONTYPE=".$userRelationType."\n";
print LEXICONTXT "DESCRIPTION=".$userDescription."\n";

print LEXICONTXT "DESCR:label:cat=catégorie\n";
my @keysExpand = sort { $a cmp $b } keys(%labels2expand);
foreach my $keyExpand (@keysExpand){
	my @keysValuesExpand = sort { $a cmp $b } keys(%{$labels2expand{$keyExpand}});
	foreach my $keyValueExpand (@keysValuesExpand){
		print LEXICONTXT "DESCR:value(".$keyExpand."):".$keyValueExpand."\n";
	}
}
close(LEXICONTXT);
################################## FIN ECRITURE SPEC TXT #######################################

close(LOG);