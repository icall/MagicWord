Installation & configuration
============================

Requirements
------------
* docker-ce :
   * https://docs.docker.com/install/linux/docker-ce/ubuntu/
   * https://docs.docker.com/install/linux/docker-ce/debian/
   * When installing docker-ce, if you are using a ubuntu based distro (such as Mint), in step 4 of the ubuntu/debian install replace  ```$(lsb_release -cs) \``` with the actual name of the ubuntu/debian distro your OS is based upon (For [Mint Tessa](https://linuxmint.com/download_all.php), it would be bionic, for [bunsen hydrogen](https://www.bunsenlabs.org/repositories.html), it would be jessie)
* docker-compose

Installation
--------------
```
git clone https://gricad-gitlab.univ-grenoble-alpes.fr/lzbk/MagicWord.git
cd MagicWord
cp .env.dist .env
vi .env
#vi or any other text editor, this will allow you to set the DB name & password
#these informations will be asked during the install
sudo docker-compose up --build -d
sudo docker-compose exec apache bash
make install
```

Notes
-----
* During the last docker command, you should be able to leave the default value for every parameter by pressing ```enter```, except for:
  * the ```database_password``` which should match the one you entered in .env file and in the PWD var
  * the ```secret``` phrase, which should be changed
* if you let default values, MW should be available at localhost:666 and the adminer at localhost:667
* You'd better leave default values, some stuff is hardcoded...

* Once application installed, you can generate some grid with these commands in the php container. It will generates 10 grids in french and english with at least 150 (and 250 for french) foundable forms.
```
php bin/console magicword:generate-grid english 10 150 0
php bin/console magicword:generate-grid french 10 250 0
```

Update
------
```
git pull origin master
sudo docker-compose up --build -d
sudo docker-compose exec php bash
make update
```

Import lexicon
------
copy `lexicon.tsv` and `spec.txt` in a new folder in `application/data/lexicons`
```
sudo docker-compose exec php bash
console lexicon:import FOLDERNAME
```

Create Administrator
--------------------
To have an administrator of your Magic Word instance,
1. you first need to create an account on the site, like any other account.
2. then launch php container `sudo docker-compose exec php bash`
3. in that container type `php bin/console fos:user:promote`
    1. enter the screen name of the account to promote
    1. submit the role `ROLE_ADMIN`
