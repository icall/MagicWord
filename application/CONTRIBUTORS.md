Contributions (since 2014)
------------
### Code
* [Arnaud Bey](http://litt-arts.univ-grenoble-alpes.fr/fr/membres/m-arnaud-bey--4727.kjsp)
* [Sylvain Hatier](https://www.researchgate.net/profile/Sylvain_Hatier)
* [Pauline Rebourgeon](https://www.linkedin.com/in/paulinerebourgeon)
* [Giacomo Mambelli](https://fr.linkedin.com/in/giacomo-mambelli)
* [David Graceffa](https://fr.linkedin.com/in/david-graceffa-155661a3)
* [Maryam Nejat](https://ca.linkedin.com/in/maryam-nejat-0758b7124)
* [Christine Lutian](https://fr.linkedin.com/in/christine-lutian-932b9282)
* [Mathieu Loiseau](http://wiki.lezinter.net/index.php/Utilisateur:Mathieu_Loiseau)
* [Joris Bouderbala](https://www.cvwanted.com/joris_bouderbala)
* [Benjamin Abrial](http://www.ens-lyon.fr/lecole/nous-connaitre/annuaire/benjamin-abrial)
* Judith Chambre

### Design and experiments
* [Mathieu Loiseau](http://wiki.lezinter.net/index.php/Utilisateur:Mathieu_Loiseau)
* [Virginie Zampa](http://virginie.zampa.free.fr/)
* [Racha Hallal](https://www.linkedin.com/in/racha-hallal-001911b8/)
* [Pauline Ballot](https://es.linkedin.com/in/pauline-ballot)
* [Arnaud Bey](http://litt-arts.univ-grenoble-alpes.fr/fr/membres/m-arnaud-bey--4727.kjsp?RH=LITTEARTSFR_MEMB03)
* [Francesca Bianco](https://www.researchgate.net/profile/Francesca_Bianco7)
* [Richard Boualavong](https://fr.linkedin.com/in/richard-boualavong-a678b3145)
* [Joris Bouderbala](https://www.cvwanted.com/joris_bouderbala)
* [Cristiana Cervini](https://www.researchgate.net/profile/Cristiana_Cervini)
* [Christine Lutian](https://fr.linkedin.com/in/christine-lutian-932b9282)
* [Agnès Montaufier](https://www.linkedin.com/in/agn%C3%A8s-montaufier-4a281b135/)
* [Pauline Rebourgeon](https://www.linkedin.com/in/paulinerebourgeon)
* [Haydée Silva](https://unam.academia.edu/Hayd%C3%A9eSilva)
* [Montiya Phoungsub](https://www.cmu.ac.th/en/faculty/humanities/teacher/5920b963-721a-44bc-a3e0-388fef8828d3#cphPageContent_wucPageFacTeacherDepartment_wucPageFacTeacherCol3_rptList_ImageCover_13)
* Timothée Liotard
* [Yoann](https://www.researchgate.net/profile/Yoann_Goudin) [Goudin](https://www.researchgate.net/profile/Goudin_Yoann)

### Graphic Design
* [David Fraisse](http://www.davidfraisse.com)
