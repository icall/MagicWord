<?php

namespace LexiconBundle\Manager;

use Doctrine\ORM\EntityManagerInterface;
use JMS\DiExtraBundle\Annotation as DI;
use LexiconBundle\Entity\Language;
use LexiconBundle\Manager\LetterManager;
use LexiconBundle\Manager\WordManager;
use MagicWordBundle\Manager\Rules\WordLengthManager;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

/**
 * @DI\Service("lexicon_manager.bigram")
 */
class BigramManager
{
    public function generateBigrams($bigrams, $pathLexiconDir)
    {
        //hash enregistrant les bigrams déjà écrit à dans $string2print
        $grambigrams = [];
        //Impression des keys
        $string2print="Bigrammes\tFreq\tPoids\tArrondi\n";
        arsort($bigrams);
        $max = max($bigrams);
        $nbTranches = 100;#nombre de tranches pour les poids de bigram (on divise $max par $nbtranches et le résultat vaut poids)
        $factorBigram=$max/$nbTranches;
        foreach ($bigrams as $key => $value) {
            $yek = $this->mb_strrev($key);
            if (!in_array($key, $grambigrams)) {
                $freqInverse=0;
                if (isset($bigrams[$yek])) {
                    $freqInverse=$bigrams[$yek];
                }
                $poids = ($value+$freqInverse)/$factorBigram;
                $poidsRound = 1;
                if ($poids>1) {
                    $poidsRound=round($poids);
                }
                $string2print .= $key."\t".$value."\t".$poids."\t".$poidsRound."\n";
                $grambigrams[] = $key;
                $grambigrams[] = $yek;
            }
        }
        file_put_contents($pathLexiconDir."/bigrams.txt", $string2print);

        return;
    }

    public function mb_strrev($string, $encoding = null)
    {
        if ($encoding === null) {
            $encoding = mb_detect_encoding($string);
        }

        $length = mb_strlen($string, $encoding);
        $reversed = '';
        while ($length-- > 0) {
            $reversed .= mb_substr($string, $length, 1, $encoding);
        }

        return $reversed;
    }
}
