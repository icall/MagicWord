<?php

namespace LexiconBundle\Manager;

use LexiconBundle\Entity\Language;
use LexiconBundle\Entity\Letter;
use LexiconBundle\Entity\Word;
use LexiconBundle\Entity\WordStart;
use MagicWordBundle\Entity\Rules\WordLength;
use JMS\DiExtraBundle\Annotation as DI;

/**
 * @DI\Service("lexicon_manager.word")
 */
class WordManager
{
    private $em;

    /**
     * @DI\InjectParams({
     *      "em" = @DI\Inject("doctrine.orm.entity_manager"),
     * })
     */
    public function __construct($em)
    {
        $this->em = $em;
    }

    public function create(Language $language, $root, $features, $value, $cleanValue)
    {
        $word = new Word;
        $word->setLanguage($language);
        $word->setRoot($root);
        foreach ($features as $feature) {
            $word->addFeature($feature);
        }
        $word->setValue($value);
        $word->setCleanValue($cleanValue);

        $this->em->persist($word);

        return;
    }

    public function checkExistence($word, Language $language)
    {
        $word = $this->em->getRepository('LexiconBundle:Word')->getByLanguageAndContentOrCleaned($language, $word);

        return $word;
    }

    public function createStarts(Language $language, $wordStarts)
    {
        $languageId = $language->getId();
        foreach ($wordStarts as $wordStartValue) {
            if (!$found = $this->em->getRepository(WordStart::class)->findOneBy(["language" => $language, "value" => $wordStartValue])) {
                $wordStart = new WordStart;
                $wordStart->setLanguage($languageId);
                $wordStart->setValue($wordStartValue);
                $this->em->persist($wordStart);
            }
        }

        return;
    }

    public function recalculate(Word $word)
    {
        $language = $word->getLanguage();
        $wordPoints=0;
        $str=$word->getCleanValue();
        $chars = preg_split('//u', $str, null, PREG_SPLIT_NO_EMPTY);
        foreach ($chars as $char) {
            if ($pointChar = $this->em->getRepository(Letter::class)->findOneBy(["language" => $language, "value" => $char])) {
                $wordPoints += $pointChar->getPoints();
            } else {
                $wordPoints += 1;
            }
        }
        $length = mb_strlen($str);
        if ($pointLength = $this->em->getRepository(WordLength::class)->findOneBy(["language" => $language, "length" => $length])) {
            $wordPoints += $pointLength->getPoints();
        }

        return $wordPoints;
    }
}
