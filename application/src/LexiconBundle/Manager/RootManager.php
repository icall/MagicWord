<?php

namespace LexiconBundle\Manager;

use LexiconBundle\Entity\Language;
use LexiconBundle\Entity\Root;
use Doctrine\ORM\EntityManagerInterface;
use JMS\DiExtraBundle\Annotation as DI;

/**
 * @DI\Service("lexicon_manager.root")
 */
class RootManager
{
    private $em;

    /**
     * @DI\InjectParams({
     *      "em" = @DI\Inject("doctrine.orm.entity_manager"),
     * })
     */
    public function __construct($em)
    {
        $this->em = $em;
    }

    public function findOrCreate(Language $language, $value, &$roots)
    {
        if (!isset($roots[$value])) {
            $root = $this->em->getRepository(Root::class)->findOneBy([
              "value" => $value,
              "language" => $language
            ]);

            if (!$root) {
                $root = new Root;
                $root->setLanguage($language);
                $root->setValue($value);

                $this->em->persist($root);
                $roots[$value] = $root;
            }

            return $root;
        }

        return $roots[$value];
    }
}
