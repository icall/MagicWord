<?php

namespace LexiconBundle\Manager;

use Doctrine\ORM\EntityManagerInterface;
use JMS\DiExtraBundle\Annotation as DI;
use LexiconBundle\Entity\Language;
use MagicWordBundle\Form\Type\LanguageType;
use Symfony\Component\HttpFoundation\Request;

/**
 * @DI\Service("lexicon_manager.language")
 */
class LanguageManager
{
    private $em;
    private $formFactory;

    /**
     * @DI\InjectParams({
     *      "em" = @DI\Inject("doctrine.orm.entity_manager"),
     *      "formFactory" = @DI\Inject("form.factory"),
     * })
     */
    public function __construct($em, $formFactory)
    {
        $this->em = $em;
        $this->formFactory = $formFactory;
    }

    public function create($languageProperty)
    {
        $language = new Language;
        $language->setValue($languageProperty["value"]);
        $language->setDescription($languageProperty["description"]);
        $language->setRelationType($languageProperty["relationtype"]);
        $language->setDirectory($languageProperty["directory"]);

        $this->em->persist($language);
        $this->em->flush();

        return $language;
    }

    public function getWordLengthForm(Language $language)
    {
        $form = $this->formFactory->createBuilder(LanguageType::class, $language)->getForm()->createView();

        return $form;
    }

    public function handleForm(Language $language, Request $request)
    {
        $form = $this->formFactory->createBuilder(LanguageType::class, $language)->getForm();
        $form->handleRequest($request);

        if ($form->isValid()) {
            $this->em->persist($language);
            $this->em->flush();
        }

        return;
    }

    public function getBigrams(Language $language)
    {
        $fileBigrams = $language->getDirectory().DIRECTORY_SEPARATOR."bigrams.txt";

        $bigrams = file_exists($fileBigrams)
          ? file($fileBigrams)
          : [];

        return $bigrams;
    }
}
