<?php

namespace LexiconBundle\Manager;

use JMS\DiExtraBundle\Annotation as DI;
use LexiconBundle\Entity\Language;
use Symfony\Component\Stopwatch\Stopwatch;

/**
 * @DI\Service("lexicon_manager.import")
 */
class ImportManager
{
    private $dir;
    private $wm;
    private $fm;
    private $lm;
    private $rm;
    private $rulesManager;
    private $em;
    private $letterm;
    private $bgm;

    /**
     * @DI\InjectParams({
     *      "dir" = @DI\Inject("%languageDir%"),
     *      "em" = @DI\Inject("doctrine.orm.entity_manager"),
     *      "wm"   = @DI\Inject("lexicon_manager.word"),
     *      "fm"   = @DI\Inject("lexicon_manager.feature"),
     *      "lm"   = @DI\Inject("lexicon_manager.language"),
     *      "rm"   = @DI\Inject("lexicon_manager.root"),
     *      "bgm"   = @DI\Inject("lexicon_manager.bigram"),
     *      "letterm"   = @DI\Inject("lexicon_manager.letter"),
     *      "rulesManager"   = @DI\Inject("mw_manager.rules"),
     * })
     */
    public function __construct($dir, $em, $wm, $fm, $lm, $rm, $rulesManager, $bgm, $letterm)
    {
        $this->dir = $dir;
        $this->em = $em;
        $this->wm = $wm;
        $this->fm = $fm;
        $this->lm = $lm;
        $this->rm = $rm;
        $this->bgm = $bgm;
        $this->letterm = $letterm;
        $this->rulesManager = $rulesManager;
    }

    public function importLexicon($pathLexiconDir)
    {
        $pathLexiconDir = $this->dir.DIRECTORY_SEPARATOR."data/lexicons".DIRECTORY_SEPARATOR.$pathLexiconDir;

        // Gestion des specs
        $pathFileSpec = $pathLexiconDir.DIRECTORY_SEPARATOR."spec.txt";
        $linesSpec = file($pathFileSpec);
        $specs = $this->parseTXTspec($linesSpec, $pathLexiconDir);

        // Gestion du lexique
        $pathFileLexicon = $pathLexiconDir.DIRECTORY_SEPARATOR."lexicon.tsv";
        $this->parseTSVlexicon($pathFileLexicon, $specs);

        return;
    }

    public function parseTXTspec($linesSpec, $pathLexiconDir)
    {
        $specs=[];
        $rewriteFrom=[];
        $rewriteTo=[];
        $weights=[];
        $lengths=[];
        $combos=[];
        $languageProperty=[];
        $languageProperty["directory"]=$pathLexiconDir;
        $specs["directory"]=$pathLexiconDir;
        foreach ($linesSpec as $line) {
            if (preg_match_all("/^LANGUAGE=(.+)\s*$/", $line, $matches)) {
                $languageProperty["value"]=$matches[1][0];
            } elseif (preg_match_all("/^RELATIONTYPE=(.+)\s*$/", $line, $matches)) {
                $languageProperty["relationtype"]=$matches[1][0];
            } elseif (preg_match_all("/^DESCRIPTION=(.+)\s*$/", $line, $matches)) {
                $languageProperty["description"]=$matches[1][0];
                $language = $this->lm->create($languageProperty);
                $specs["language"] = $language;
                $specs["language_id"] = $language->getId();
            } elseif (preg_match_all("/^RW:(.+)=(.*)\s*$/", $line, $matches)) {
                $charactersFrom=explode(",", $matches[1][0]);
                $characterTo=$matches[2][0];
                foreach ($charactersFrom as $characterFrom) {
                    $rewriteFrom[] = $characterFrom;
                    $rewriteTo[] = $characterTo;
                }
            } elseif (preg_match_all("/^DESCR:label:(.+)=(.+)\s*$/", $line, $matches)) {
                $specs["DESCR:label"][$matches[1][0]]=$matches[2][0];
            } elseif (preg_match_all("/^DESCR:label:(.+)=(.+)\s*$/", $line, $matches)) {
                $specs["DESCR:feature"][$matches[1][0]]=$matches[2][0];
            } elseif (preg_match_all("/^WEIGHT:(.+)\s*$/", $line, $matches)) {
                $weightsNletters=explode(",", $matches[1][0]);
                foreach ($weightsNletters as $weightNletter) {
                    $tab=explode("=", $weightNletter);
                    $weights[$tab[0]]=$tab[1];
                }
            } elseif (preg_match_all("/^LENGTH:(.+)\s*$/", $line, $matches)) {
                $weightsNLengths = explode(",", $matches[1][0]);
                foreach ($weightsNLengths as $weightsNLength) {
                    $tab = explode("=", $weightsNLength);
                    $lengths[$tab[0]] = $tab[1];
                    $this->rulesManager->findOrCreateWordLength($language, $tab[0], $tab[1]);
                }
            } elseif (preg_match_all("/^COMBOPOINTS:(.+)\s*$/", $line, $matches)) {
                $comboNpoints = explode(",", $matches[1][0]);
                foreach ($comboNpoints as $comboNpoint) {
                    $tab = explode("=", $comboNpoint);
                    $combos[$tab[0]] = $tab[1];
                    $this->rulesManager->findOrCreateCombo($language, $tab[0], $tab[1]);
                }
            }
        }
        $specs["rewriteFrom"] = $rewriteFrom;
        $specs["rewriteTo"] = $rewriteTo;
        $specs["weights"] = $weights;
        $specs["lengths"] = $lengths;
        $specs["combos"] = $combos;

        return $specs;
    }

    public function parseTSVlexicon($pathFileLexicon, $specs)
    {
        $this->em->getConnection()->getConfiguration()->setSQLLogger(null);

        $language = $specs["language"];
        $weights = $specs["weights"];
        $lengths = $specs["lengths"];
        $pathLexiconDir = $specs["directory"];
        $roots = [];
        $featuresToFlush = [];
        $letters = [];
        $wordStarts = [];
        $flushCpt = 0;
        $cpt = 0;
        $maxToFlush = 5000;
        $total = count(file($pathFileLexicon));
        $stopwatch = new Stopwatch();
        $bigrams=[];
        $stopwatchName = uniqid();

        $stopwatch->start("global_import");
        $stopwatch->start($stopwatchName);

        $handle = @fopen($pathFileLexicon, "r");
        if ($handle) {
            while (($line = fgets($handle, 4096)) !== false) {
                if (preg_match_all("/^([^\t]+)\t([^\t]+)\t([^\t]+)\s*$/", $line, $matches)) {
                    $uid1 = uniqid();
                    $uid2 = uniqid();

                    $wordValue = $matches[1][0];
                    $rootValue = $matches[2][0];
                    #mb_eregi_replace ?
                    $cleanWordValue = str_replace($specs["rewriteFrom"], $specs["rewriteTo"], $wordValue);
                    // $cleanWordValue = preg_replace($specs["rewriteFrom"], $specs["rewriteTo"], $wordValue);
                    $cleanWordValue = trim($cleanWordValue);
                    $string2print="ROOT = ".$rootValue." / CLEAN = ".$cleanWordValue;
                    //$cleanWordValue = preg_replace("/\P{L}/", "", $cleanWordValue);#bug pour le russe
                        $cleanWordValue = mb_eregi_replace("/\P{L}/", "", $cleanWordValue);#fonctionne pour le russe
                        $string2print.=" / EREGI = ".$cleanWordValue."\n";

                    // SI après nettoyage, la cleanvalue contient moins de deux caractères, on passe à la ligne suivante
                    //
                    // Gestion des lettres et débuts de mots
                    $wordsLetters = preg_split('//u', $cleanWordValue, null, PREG_SPLIT_NO_EMPTY);
                    if ((count($wordsLetters)>=2) && (count($wordsLetters)<=16)) {

                        // ROOT
                        $root = $this->rm->findOrCreate($language, $rootValue, $roots);

                        // FEATURES
                        $labelsNValues = explode(",", $matches[3][0]);
                        $features = [];
                        foreach ($labelsNValues as $labelNValue) {
                            $featureStringTab = explode("=", $labelNValue);
                            $features[] = $this->fm->findOrCreate($language, trim($featureStringTab[0]), trim($featureStringTab[1]), $featuresToFlush);
                        }

                        // WORD
                        $this->wm->create($language, $root, $features, $wordValue, $cleanWordValue);


                        $wordStartString = "";
                        $previousLetter="";
                        foreach ($wordsLetters as $wordLetter) {
                            $wordStartString .= $wordLetter;
                            if (!in_array($wordStartString, $wordStarts) && mb_strlen($wordStartString) > 1) {
                                $wordStarts[] = $wordStartString;
                            }
                            if (!in_array($wordLetter, $letters)) {
                                $letters[] = $wordLetter;
                            }
                            //Gestion des $bigram
                            if ($previousLetter != "") {
                                $bigramString = $previousLetter.$wordLetter;
                                $bigrams[trim($bigramString)] = !array_key_exists($bigramString, $bigrams) ? 1 : $bigrams[$bigramString] + 1;
                            }
                            $previousLetter = $wordLetter;
                        }
                    }
                    if ($flushCpt == $maxToFlush) {
                        $this->wm->createStarts($language, $wordStarts);
                        $wordStarts = null;
                        $wordStarts = [];
                        $this->flushAndFreeMemory();
                        $flushCpt = 1;
                        $roots = null;
                        $roots = [];
                        $featuresToFlush = null;
                        $featuresToFlush = [];
                        $languageId = $specs["language_id"];
                        $language = $this->em->getRepository(Language::class)->find($languageId);
                        $percent = round($cpt / $total * 100, 2);
                        echo("[".$percent."%]  ".$wordValue."\n");
                        $event = $stopwatch->stop($stopwatchName);

                        echo "max memory > " . $event->getMemory()/1048576 . " MB \n";
                        echo "duration > " . $event->getDuration()/1000 . " seconds \n\n";

                        $stopwatchName = uniqid();
                        $stopwatch->start($stopwatchName);
                    }
                }
                $cpt++;
                $flushCpt++;
            }
            fclose($handle);
        }

        $this->bgm->generateBigrams($bigrams, $pathLexiconDir);
        echo("Bigram OK \n");
        $this->wm->createStarts($language, $wordStarts);
        echo("Starts OK \n");
        $this->letterm->create($language, $letters, $weights);
        echo("Lettre OK \n");
        $this->flushAndFreeMemory();
        echo("Flush OK \n");

        $event = $stopwatch->stop("global_import");
        echo "global max memory > " . $event->getMemory()/1048576 . " MB \n";
        echo "global duration > " . $event->getDuration()/1000 . " seconds \n\n";

        return;
    }

    private function flushAndFreeMemory()
    {
        $this->em->flush();
        gc_collect_cycles();
        $this->em->clear();

        return;
    }
}
