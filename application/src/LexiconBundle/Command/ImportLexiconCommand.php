<?php

namespace LexiconBundle\Command;

use LexiconBundle\Manager\ImportManager;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;

class ImportLexiconCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
          ->setName('lexicon:import')
          ->setDescription('Put your files, named exactly "lexicon.tsv" & "spec.txt" in a directory (ex:/euskara) in application/data/lexicons')
          ->addArgument('directory', InputArgument::REQUIRED, 'name of directory in application/data/lexicons');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $pathLexiconDir = $input->getArgument('directory');

        if ($pathLexiconDir) {
            echo 'You passed an argument: ' . $pathLexiconDir . "\n\n";
            $importManager = $this->getContainer()->get('lexicon_manager.import');
            $importManager->importLexicon($pathLexiconDir);
        } else {
            echo 'Argument missing';
        }
    }
}
