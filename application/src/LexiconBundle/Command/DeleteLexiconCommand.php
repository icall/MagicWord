<?php

namespace LexiconBundle\Command;

use LexiconBundle\Entity\Language;
use LexiconBundle\Entity\Root;
use LexiconBundle\Entity\Word;
use LexiconBundle\Entity\WordStart;
use LexiconBundle\Manager\ImportManager;
use MagicWordBundle\Entity\FoundableForm;
use MagicWordBundle\Entity\Game;
use MagicWordBundle\Entity\Grid;
use MagicWordBundle\Entity\Objective;
use MagicWordBundle\Entity\Round;
use MagicWordBundle\Entity\Score;
use MagicWordBundle\Entity\Wordbox\Acquisition;
use MagicWordBundle\Entity\WrongForm;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\Question;
use Symfony\Component\Filesystem\Filesystem;

class DeleteLexiconCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
          ->setName('lexicon:delete')
          ->setDescription('Delete lexicon from db & bigram.txt files');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $em = $this->getContainer()->get('doctrine')->getEntityManager('default');
        $helper = $this->getHelper('question');

        $question = new Question("Please enter the ID of the lexicon \n");
        $languages = $em->getRepository(Language::class)->findAll();
        foreach ($languages as $language) {
            $output->writeln('<comment>'.$language->getId()." > ".$language->getValue().'</comment>');
        }

        $idLexicon = $helper->ask($input, $output, $question);

        if ($idLexicon) {
            $language = $em->getRepository(Language::class)->find($idLexicon);
            if ($language) {
                $output->writeln('<info>############ SUPPRESSION LANGUE #############</info>');

                echo "Suppression ".$language->getValue()."\n";

                $em->getRepository(Acquisition::class)->deleteByLanguage($language);
                $output->writeln('<comment>Suppression acquisitions</comment>');


                $games = $em->getRepository(Game::class)->findByLanguage($language);
                foreach ($games as $game) {
                    $scores =  $em->getRepository(Score::class)->findByGame($game);
                    foreach ($scores as $score) {
                        $em->remove($score);
                        foreach ($score->getActivities() as $activity) {
                            $em->remove($activity);
                        }
                    }
                }

                $em->flush();
                $output->writeln('<comment>Suppression activity ok</comment>');
                $output->writeln('<comment>Suppression score ok</comment>');
                // delete games
                $em->getRepository(Game::class)->deleteByLanguage($language);
                $output->writeln('<comment>Suppression game ok</comment>');

                // delete grid
                $rounds = $em->getRepository(Round::class)->findByLanguage($language);
                foreach ($rounds as $round) {
                    $em->remove($round);
                }
                $em->flush();
                $output->writeln('<comment>Suppression Round ok</comment>');

                // delete grid
                $grids = $em->getRepository(Grid::class)->findByLanguage($language);
                foreach ($grids as $grid) {
                    $em->remove($grid);
                }
                $em->flush();
                $output->writeln('<comment>Suppression grilles ok</comment>');

                $words = $em->getRepository(Word::class)->deleteByLanguage($language);
                $output->writeln('<comment>Suppression words ok</comment>');

                $roots = $em->getRepository(Root::class)->deleteByLanguage($language);
                $output->writeln('<comment>Suppression roots ok</comment>');

                $wordstarts = $em->getRepository(WordStart::class)->deleteByLanguage($language->getId());
                $output->writeln('<comment>Suppression wordstarts ok</comment>');

                $wrongForms = $em->getRepository(WrongForm::class)->findByLanguage($language);
                foreach ($wrongForms as $wrongForm) {
                    $em->remove($wrongForm);
                }
                $em->flush();
                $output->writeln('<comment>Suppression wrongForm ok</comment>');


                $em->remove($language);
                $output->writeln('<comment>Suppression langage ok</comment>');
                $em->flush();

                //suppression fichier bigram
                $pathProject = $this->getContainer()->getParameter('languageDir');

                $bigramPath = $pathProject."/data/lexicons/".$language->getValue().DIRECTORY_SEPARATOR."bigrams.txt";
                $fileSystem = new Filesystem();
                $fileSystem->remove($bigramPath);
                $output->writeln('<comment>Suppression fichier bigrammes</comment>');

                //TODO supp acquisition, et rules (ComboPoints et WordLength)

                $output->writeln('<info>########### SUPPRESSION LANGUE OK ############</info>');
            } else {
                $output->writeln('<comment>Pas de langue avec cet ID</comment>');
            }
        } else {
            echo 'Argument missing (language id)';
        }
    }
}
