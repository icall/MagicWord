<?php

namespace MagicWordBundle\Form\Type\ObjectiveType;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Doctrine\ORM\EntityRepository;

class FeatureType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $languageId = $options["languageId"];

        $builder->add('feature', EntityType::class, [
          'class' => 'LexiconBundle:Feature',
          'query_builder' => function ($repo) use ($languageId) {
              $qb = $repo->createQueryBuilder('f');
              $qb->andWhere('f.language = '.$languageId);
              $qb->orderBy('f.value', 'ASC');

              return $qb;
          },
          'group_by' => 'label',
          'placeholder' => '- feature -',
          'required' => true,
          'empty_data' => null,
          'choice_label' => 'value',
          'attr' => array('class' => 'form-control'),
        ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'LexiconBundle\Entity\Feature',
            'languageId' => null
        ]);
    }
}
