<?php

namespace MagicWordBundle\Controller\Lexicon;

use LexiconBundle\Entity\Root;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class RootController extends Controller
{
    /**
     * @Route("/root/{id}", name="root")
     */
    public function getInfosAction(Root $root)
    {
        return $this->render('MagicWordBundle:Lexicon:root.html.twig', ['root' => $root]);
    }
}
