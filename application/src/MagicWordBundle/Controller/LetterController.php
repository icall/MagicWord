<?php

namespace MagicWordBundle\Controller;

use LexiconBundle\Entity\Language;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class LetterController extends Controller
{
    /**
     * @Route("/get-letters/{id}", name="get_letters", options={"expose"=true})
     * @Method("POST")
     */
    public function getLettersAction(Language $language, Request $request)
    {
        $squares = $request->request->get('squares');

        $grid = [];
        $x = 0;
        for ($i=0; $i < 4; $i++) {
            for ($j=0; $j < 4; $j++) {
                $grid[$i][$j] = (($squares[$x] != " ") && ($squares[$x] != "-") && ($squares[$x] != "_") && ($squares[$x] != ""))
                  ? $squares[$x]
                  :  null;
                $x++;
            }
        }

        $letters = $this->get('mw_manager.grid')->lottery($language, 4, null, null, $grid);

        return new JsonResponse($letters);
    }
}
