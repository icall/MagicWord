<?php

namespace MagicWordBundle\Controller;

use LexiconBundle\Entity\Language;
use MagicWordBundle\Entity\FoundableForm;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class FoundableFormController extends Controller
{
    /**
     * @Route("/found/{id}", name="found")
     */
    public function displayFoundAction(Language $language)
    {
        $user = $this->get('security.token_storage')->getToken()->getUser();
        $activities = $this->getDoctrine()->getRepository('MagicWordBundle:Activity')->foundByUserAndLanguage($user, $language);
        $foundable = $this->getDoctrine()->getRepository('MagicWordBundle:FoundableForm')->foundableByLanguage($language);

        $found = [];
        foreach ($activities as $activity) {
            foreach ($activity->getFoundForms() as $form) {
                $found[] = $form;
            }
        }

        return $this->render('MagicWordBundle:FoundableForm:found.html.twig', [
            'found' => $found,
            'language' => $language,
            'foundable' => $foundable
        ]);
    }

    /**
     * @Route("/form/{formId}/{languageId}", name="form_details")
     * @ParamConverter("foundableForm", class="MagicWordBundle:FoundableForm",  options={"id" = "formId"})
     * @ParamConverter("language", class="LexiconBundle:Language", options={"id" = "languageId"})
     */
    public function displayFormDetailsAction(FoundableForm $foundableForm, Language $language)
    {
        $foundCount = $this->getDoctrine()->getRepository('MagicWordBundle:FoundableForm')->foundCountByFormAndLanguage($foundableForm, $language);
        $userCount = count($this->getDoctrine()->getRepository('MagicWordBundle:Player')->findAll());

        return $this->render('MagicWordBundle:FoundableForm:details.html.twig', [
            'foundableForm' => $foundableForm,
            'language' => $language,
            'foundCount' => $foundCount,
            'userCount' => $userCount
        ]);
    }
}
