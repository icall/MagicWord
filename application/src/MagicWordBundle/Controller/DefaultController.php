<?php

namespace MagicWordBundle\Controller;

use Michelf\Markdown;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    /**
     * @Route("/home", name="home")
     */
    public function homeAction()
    {
        return $this->render('MagicWordBundle:Default:home.html.twig');
    }

    /**
     * @Route("/home/play", name="home_play")
     */
    public function playAction()
    {
        return $this->render('MagicWordBundle:Default:play.html.twig');
    }

    /**
     * @Route("/home/create", name="home_create")
     */
    public function createAction()
    {
        return $this->render('MagicWordBundle:Default:create.html.twig');
    }

    /**
     * @Route("/home/me", name="home_me")
     */
    public function meAction()
    {
        return $this->render('MagicWordBundle:Default:me.html.twig');
    }

    /**
     * @Route("/", name="index")
     */
    public function indexAction()
    {
        $contributors = Markdown::defaultTransform(file_get_contents("files/CONTRIBUTORS.md"));

        return $this->render('MagicWordBundle:Default:index.html.twig', ['contributors' => $contributors]);
    }
}
