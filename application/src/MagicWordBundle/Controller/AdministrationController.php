<?php

namespace MagicWordBundle\Controller;

use LexiconBundle\Entity\Language;
use LexiconBundle\Entity\Letter;
use LexiconBundle\Entity\Root;
use LexiconBundle\Entity\Word;
use LexiconBundle\Entity\WordStart;
use MagicWordBundle\Entity\Grid;
use MagicWordBundle\Entity\Rules\ComboPoints;
use MagicWordBundle\Entity\Rules\WordLength;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class AdministrationController extends Controller
{
    /**
     * @Route("/administration", name="admin_index")
     */
    public function indexAction()
    {
        return $this->render('MagicWordBundle:Administration:index.html.twig');
    }

    /**
     * @Route("/administration/{id}/index", name="index_language")
     */
    public function indexLanguageAction(Language $language)
    {
        return $this->render('MagicWordBundle:Administration:index-language.html.twig', ['language' => $language]);
    }

    /**
     * @Route("/administration/{id}/parameters", name="language_parameters")
     * @Method("GET")
     */
    public function parametersLanguageAction(Language $language)
    {
        $form = $this->get('lexicon_manager.language')->getWordLengthForm($language);

        return $this->render('MagicWordBundle:Administration:language-edit.html.twig', ['form' => $form, 'language' => $language]);
    }

    /**
     * @Route("/administration/{id}/debug", name="language_debug")
     * @Method("GET")
     */
    public function debugLanguageAction(Language $language)
    {
        $bestGrids = $this->getDoctrine()->getRepository(Grid::class)->getBestByLanguage($language);
        $worstGrids = $this->getDoctrine()->getRepository(Grid::class)->getWorstByLanguage($language);
        $countGrids = $this->getDoctrine()->getRepository(Grid::class)->countAllByLanguage($language);
        $countWords = $this->getDoctrine()->getRepository(Word::class)->countAllByLanguage($language);
        $countRoots = $this->getDoctrine()->getRepository(Root::class)->countAllByLanguage($language);
        $countWordStarts = $this->getDoctrine()->getRepository(WordStart::class)->countAllByLanguage($language);
        $bigrams = $this->get('lexicon_manager.language')->getBigrams($language);
        $bigrams =  array_slice($bigrams, 1, 6);
        $letters = $this->getDoctrine()->getRepository(Letter::class)->findBy(["language" => $language], ["value" => "ASC"]);

        return $this->render('MagicWordBundle:Administration:language-debug.html.twig', [
          'language' => $language,
          'bestGrids' => $bestGrids,
          'worstGrids' => $worstGrids,
          'countGrids' => $countGrids,
          'countWords' => $countWords,
          'countWordStarts' => $countWordStarts,
          'countRoots' => $countRoots,
          'letters' => $letters,
          'bigrams' => $bigrams
        ]);
    }

    /**
     * @Route("/administration/{id}/parameters", name="language_parameters_submit")
     * @Method("POST")
     */
    public function parametersLanguageSubmitAction(Language $language, Request $request)
    {
        $this->get('lexicon_manager.language')->handleForm($language, $request);

        return $this->redirectToRoute('index_language', ['id' => $language->getId()]);
    }

    /**
     * @Route("/administration/{id}/wordlengths", name="wordlengths")
     */
    public function wordLengthAction(Language $language)
    {
        $wordlengths = $this->getDoctrine()->getRepository(WordLength::class)->findByLanguage($language);

        return $this->render('MagicWordBundle:Administration:wordlength.html.twig', ['wordlengths' => $wordlengths, 'language' => $language]);
    }

    /**
     * @Route("/administration/{id}/combopoints", name="combopoints")
     */
    public function comboPointsAction(Language $language)
    {
        $combopoints = $this->getDoctrine()->getRepository(ComboPoints::class)->findByLanguage($language);

        return $this->render('MagicWordBundle:Administration:combopoints.html.twig', ['combopoints' => $combopoints, 'language' => $language]);
    }

    /**
     * @Route("/administration/wordlength/{id}", name="wordlength_edit")
     * @Method("GET")
     */
    public function wordLengthEditAction(WordLength $wordlength)
    {
        $form = $this->get('mw_manager.rules')->getWordLengthForm($wordlength);

        return $this->render('MagicWordBundle:Administration:wordlength-edit.html.twig', ['form' => $form, 'wordlength' => $wordlength]);
    }

    /**
     * @Route("/administration/combopoint/{id}", name="combopoint_edit")
     * @Method("GET")
     */
    public function comboPointEditAction(ComboPoints $combopoint)
    {
        $form = $this->get('mw_manager.rules')->getComboPointForm($combopoint);

        return $this->render('MagicWordBundle:Administration:combopoint-edit.html.twig', ['form' => $form, 'combopoints' => $combopoint]);
    }

    /**
     * @Route("/administration/wordlength/{id}", name="wordlength_submit")
     * @Method("POST")
     */
    public function wordLengthSubmitAction(WordLength $wordlength, Request $request)
    {
        $this->get('mw_manager.rules')->handleWordLengthForm($wordlength, $request);

        return $this->redirectToRoute('wordlengths', ['id' => $wordlength->getLanguage()->getId()]);
    }
    /**
     * @Route("/administration/combopoint/{id}", name="combopoint_submit")
     * @Method("POST")
     */
    public function comboPointSubmitAction(ComboPoints $combopoint, Request $request)
    {
        $this->get('mw_manager.rules')->handleComboPointForm($combopoint, $request);

        return $this->redirectToRoute('combopoints', ['id' => $combopoint->getLanguage()->getId()]);
    }

    /**
     * @Route("/administration/letters/{id}", name="letters")
     */
    public function letterPointsAction(Language $language)
    {
        $letters = $this->getDoctrine()->getRepository('LexiconBundle:Letter')->findBy(["language" => $language], ["value" => "ASC"]);

        return $this->render('MagicWordBundle:Administration:letterslanguage.html.twig', ['letters' => $letters, 'language' => $language]);
    }

    /**
     * @Route("/administration/letter/{id}", name="letter_edit")
     * @Method("GET")
     */
    public function letterPointsEditAction(Letter $letter)
    {
        $form = $this->get('mw_manager.rules')->getLetterForm($letter);

        return $this->render('MagicWordBundle:Administration:letterslanguage-edit.html.twig', ['letter' => $letter, 'form' => $form]);
    }

    /**
     * @Route("/administration/letter/{id}", name="letter_submit")
     * @Method("POST")
     */
    public function letterPointsSubmitAction(Letter $letter, Request $request)
    {
        $this->get('mw_manager.rules')->handleLetterLanguageForm($letter, $request);

        return $this->redirectToRoute('letters', ['id' => $letter->getLanguage()->getId()]);
    }


    /**
     * @Route("/administration/general_parameters_edit", name="general_parameters_edit")
     * @Method("GET")
     */
    public function generalParametersEditAction()
    {
        $form = $this->get('mw_manager.administration')->getGeneralParametersForm();

        return $this->render('MagicWordBundle:Administration:general-parameters-edit.html.twig', ['form' => $form]);
    }


    /**
     * @Route("/administration/general_parameters_edit", name="general_parameters_submit")
     * @Method("POST")
     */
    public function generalParametersSubmitAction(Request $request)
    {
        $this->get('mw_manager.administration')->handleGeneralParametersForm($request);

        return $this->redirectToRoute('admin_index');
    }
}
