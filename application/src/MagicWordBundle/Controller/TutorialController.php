<?php

namespace MagicWordBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class TutorialController extends Controller
{
    /**
     * @Route("/tutorial", name="tutorial", options={"expose"=true})
     * @Method("GET")
     */
    public function playTutorialAction()
    {
        $generalParameters = $this->get("mw_manager.administration")->getGeneralParameters();
        $round = $generalParameters->getTutorial()->getRounds()[0];

        return $this->render('MagicWordBundle:Round:tutorial.html.twig', ['round' => $round]);
    }
}
