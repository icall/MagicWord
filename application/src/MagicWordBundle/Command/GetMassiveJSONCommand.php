<?php

namespace MagicWordBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class GetMassiveJSONCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('magicword:massive-json')
            ->setDescription('get json export for a given massive game')
            ->addArgument('massiveId', InputArgument::REQUIRED)
           ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $em = $this->getContainer()->get('doctrine')->getEntityManager('default');
        $exportManager = $this->getContainer()->get('mw_manager.export');
        $massiveId = $input->getArgument('massiveId');
        if ($massive = $em->getRepository('MagicWordBundle:GameType\Massive')->find($massiveId)) {
            $output->writeln('<info>############ JSON EXPORT (id: '.$massiveId.') #############</info>');
            $array =  $exportManager->exportGame($massive);
            $output->writeln(json_encode($array));
            $output->writeln('<info>################################################</info>');
        } else {
            $output->writeln('<error>No massive game found for this id ('.$massiveId.')</error>');
        }
    }
}
