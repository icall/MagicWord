<?php

namespace MagicWordBundle\Command;

use LexiconBundle\Entity\Language;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\Question;
use Symfony\Component\Filesystem\Filesystem;

class GenerateGridCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('magicword:generate-grid')
            ->setDescription('generate grids')
            ->addArgument('number', InputArgument::REQUIRED)
            ->addArgument('threshold', InputArgument::OPTIONAL)
            ->addArgument('minBigram', InputArgument::OPTIONAL);
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $em = $this->getContainer()->get('doctrine')->getEntityManager('default');
        $helper = $this->getHelper('question');
        $gridManager = $this->getContainer()->get('mw_manager.grid');

        $question = new Question("Please enter the ID of a lexicon \n");
        $languages = $em->getRepository(Language::class)->findAll();
        foreach ($languages as $language) {
            $output->writeln('<comment>'.$language->getId()." > ".$language->getValue().'</comment>');
        }

        $idLexicon = $helper->ask($input, $output, $question);

        $number = $input->getArgument('number');
        $threshold = $input->getArgument('threshold');
        $minBigram = $input->getArgument('minBigram');
        $totalFormCount = 0;
        $totalFormCountAll = 0;
        $keptGrid = 0;
        $allGrid = 0;
        $best = 0;
        $worst = 1000;
        $minBigram = (!$minBigram) ? null : $minBigram;

        // STATS
        $averageCombo = 0;
        $totalLemma = 0;
        $totalComboCounts = 0;
        $globalLemmas = [];

        while ($keptGrid < $number) {
            $language = $em->getRepository('LexiconBundle:Language')->find($idLexicon);
            $languageName = $language->getValue();
            $timeStart = microtime(true);
            $grid = $gridManager->generate($language, true, $minBigram);
            $timeEnd = microtime(true);
            $executionTime = round($timeEnd - $timeStart, 2);
            $foundables = $grid->getFoundableForms();
            $formCount = $foundables ? count($foundables) : 0;
            $lemmasCombos = $gridManager->countLemmasAndCombos($grid);


            // STATS
            $totalFormCountAll += $formCount;
            $averageCombo += $lemmasCombos["comboAverage"];
            $totalLemma += $lemmasCombos["lemmaCount"];
            $totalComboCounts += $lemmasCombos["comboCount"];
            $lemmas = $lemmasCombos["lemmas"];

            foreach ($lemmas as $lemma) {
                $globalLemmas[] = $lemma['root']['id'];
            }
            $globalLemmas = array_unique($globalLemmas, SORT_NUMERIC);

            $best = ($formCount > $best) ? $formCount : $best;
            $worst = ($formCount < $worst) ? $formCount : $worst;
            if ($threshold && $formCount < $threshold) {
                $output->writeln('<comment>A grid has been generated but does not contains enough forms ('.$formCount.'). (generated in '.$executionTime.' sec.)</comment>');
                $gridId = $grid->getId();
                $squares = $em->getRepository('MagicWordBundle:Square')->findByGrid($gridId);
                $foundableForms = $em->getRepository('MagicWordBundle:FoundableForm')->findByGrid($gridId);
                foreach ($squares as $square) {
                    $em->remove($square);
                }
                foreach ($foundableForms as $foundableForm) {
                    $em->remove($foundableForm);
                }
                $em->remove($grid);
                $em->flush();
            } else {
                $keptGrid++;
                $output->writeln('<info>('.$keptGrid.') A grid has been generated and contains '.$formCount.' forms from '.$lemmasCombos["lemmaCount"].' lemmas with '.$lemmasCombos["comboCount"].' combos (generated in '.$executionTime.' sec.)</info>');

                $totalFormCount += $formCount;
            }
            $em->clear();
            $allGrid++;
            $averageCombos = round($averageCombo/ $allGrid, 2);
            $averageLemmas = round($totalLemma/ $allGrid);
            $averageAll = round($totalFormCountAll / $allGrid);
            $averageComboCounts = round($totalComboCounts / $allGrid);
            $distinctLemmas = count($globalLemmas);
            $output->writeln('Count distinct lemmas : <error>'.$distinctLemmas.'</error>');
            $output->writeln('Average form count : <error>'.$averageAll.'</error>');
            $output->writeln('Average Lemma count : <error>'.$averageLemmas.'</error>');
            $output->writeln('Average combo count : <error>'.$averageComboCounts.'</error>');
            $output->writeln('Average combo length : <error>'.$averageCombos.'</error>');
            $output->writeln('Best grid : <error>'.$best.'</error>');
            $output->writeln('Worst grid : <error>'.$worst.'</error>');
            $output->writeln('');
        }

        $average = round($totalFormCount / $keptGrid);
        $output->writeln('<info>################### DONE ! ################################</info>');
        //$output->writeln('Average form count: for '. $keptGrid .' kept grids : <error>'.$average.'</error>');
        $averageAll = round($totalFormCountAll / $allGrid);
        $output->writeln('Count distinct lemmas : <error>'.$distinctLemmas.'</error>');
        $output->writeln('Average form count : <error>'.$averageAll.'</error>');
        $output->writeln('Average lemma count : <error>'.$averageLemmas.'</error>');
        $output->writeln('Average combo count : <error>'.$averageComboCounts.'</error>');
        $output->writeln('Average combo length : <error>'.$averageCombos.'</error>');
        $output->writeln('Best grid : <error>'.$best.'</error>');
        $output->writeln('Worst grid : <error>'.$worst.'</error>');
        $output->writeln('<info>###########################################################</info>');

        $file = "data/import-".$idLexicon.".csv";
        $output = $minBigram.";".$number.";".$averageAll.";".$averageLemmas.";".$distinctLemmas.";".$averageComboCounts.";".$averageCombos."\n";
        file_put_contents($file, $output, FILE_APPEND);
    }
}
