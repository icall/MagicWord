<?php

namespace MagicWordBundle\Command;

use MagicWordBundle\Entity\AccessType;
use MagicWordBundle\Entity\GeneralParameters;
use MagicWordBundle\Entity\LanguageUI;
use MagicWordBundle\Entity\RoundType\RoundType;
use MagicWordBundle\Entity\Wordbox\AcquisitionType;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class FixturesCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('magicword:fixtures')
            ->setDescription('Fixtures');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln('<info>############ FIXTURES #############</info>');
        $em = $this->getContainer()->get('doctrine')->getEntityManager('default');

        // access type
        echo "access types... \n";
        $accessTypes = ["public", "link"];
        foreach ($accessTypes as $type) {
            $accessType = new AccessType;
            $accessType->setName($type);
            $em->persist($accessType);
        }

        // general_parameters
        echo "General parameters... \n";
        $parameters = new GeneralParameters();
        $parameters->setSelfRegistration(true);
        $parameters->setHomeText("Magic Word was created as part of the GAMER work package in the Innovalangues project at the Stendhal University in Grenoble, France. Innovalangues is a language learning centered IDEFI project (Initiatives in Excellence for Innovative Education) and will run from 2012 to 2018.");
        $parameters->setFooter("footer à personnaliser");
        $em->persist($parameters);

        // language_ui
        echo "languages UI... \n";
        $languages = [
          "french" => "fr",
          "english" => "en"
        ];
        foreach ($languages as $name => $value) {
            $languageUI = new LanguageUI;
            $languageUI->setName($name);
            $languageUI->setValue($value);
        }
        $em->persist($languageUI);

        // roundType
        echo "round types... \n";
        $roundTypes = [
          "rush" => 'RoundType\\Rush',
          "conquer" => 'RoundType\\Conquer'
        ];
        foreach ($roundTypes as $name => $class) {
            $roundType = new RoundType;
            $roundType->setName($name);
            $roundType->setClass($name);
            $em->persist($roundType);
        }

        // wordbox_acquisition_type
        echo "wordbox acquisition types... \n";
        $acquisitionTypes = ["manual"];
        foreach ($acquisitionTypes as $type) {
            $acquisitionType = new AcquisitionType;
            $acquisitionType->setValue($type);
            $em->persist($acquisitionType);
        }
        $em->flush();
        $output->writeln('<info>########## FIXTURES DONE ###########</info>');
    }
}
