<?php

namespace MagicWordBundle\Command;

use LexiconBundle\Entity\Language;
use LexiconBundle\Entity\Root;
use LexiconBundle\Entity\Word;
use LexiconBundle\Entity\WordStart;
use LexiconBundle\Manager\ImportManager;
use MagicWordBundle\Entity\FoundableForm;
use MagicWordBundle\Entity\Game;
use MagicWordBundle\Entity\Grid;
use MagicWordBundle\Entity\Objective;
use MagicWordBundle\Entity\Round;
use MagicWordBundle\Entity\Score;
use MagicWordBundle\Entity\Wordbox\Acquisition;
use MagicWordBundle\Entity\WrongForm;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\Question;
use Symfony\Component\Filesystem\Filesystem;

class DeleteGridCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
          ->setName('magicword:delete-grid')
          ->setDescription('Delete lexicon from db & bigram.txt files')
          ->addArgument('id_lexicon', InputArgument::REQUIRED, 'id of lexicon you want to delete');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln('<info>############ SUPPRESSION GRIDS #############</info>');

        $em = $this->getContainer()->get('doctrine')->getEntityManager('default');
        $idLexicon = $input->getArgument('id_lexicon');

        if ($idLexicon) {
            $language = $em->getRepository(Language::class)->find($idLexicon);
            echo 'You passed an argument: ' . $idLexicon . "\n\n";

            $em->getRepository(Acquisition::class)->deleteByLanguage($language);
            $output->writeln('<comment>Suppression acquisitions</comment>');

            $games = $em->getRepository(Game::class)->findByLanguage($language);
            foreach ($games as $game) {
                $scores =  $em->getRepository(Score::class)->findByGame($game);
                foreach ($scores as $score) {
                    $em->remove($score);
                    foreach ($score->getActivities() as $activity) {
                        $em->remove($activity);
                    }
                }
            }

            $em->flush();
            $output->writeln('<comment>Suppression activity ok</comment>');
            $output->writeln('<comment>Suppression score ok</comment>');
            // delete games
            $em->getRepository(Game::class)->deleteByLanguage($language);
            $output->writeln('<comment>Suppression game ok</comment>');

            // delete grid
            $rounds = $em->getRepository(Round::class)->findByLanguage($language);
            foreach ($rounds as $round) {
                $em->remove($round);
            }
            $em->flush();
            $output->writeln('<comment>Suppression Round ok</comment>');



            // delete grid
            $i = 0;
            $grids = $em->getRepository(Grid::class)->findByLanguage($language);
            foreach ($grids as $grid) {
                $em->remove($grid);
                $i++;

                if ($i > 500) {
                    $em->flush();
                    $i = 0;
                    $output->writeln('<comment>Suppression grilles...</comment>');
                }
            }
            $em->flush();
            $output->writeln('<info>############ SUPPRESSION GRIDS OK #############</info>');
        } else {
            echo 'Argument missing';
        }
    }
}
