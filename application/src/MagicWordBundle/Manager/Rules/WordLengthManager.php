<?php

namespace App\Manager\Rules;

use App\Entity\Lexicon\Language;
use App\Entity\Rules\WordLength;
use Doctrine\ORM\EntityManagerInterface;
use JMS\DiExtraBundle\Annotation as DI;

/**
 * @DI\Service("mw_manager.wordLength")
 */
class WordLengthManager
{
    private $em;

    /**
     * @DI\InjectParams({
     *      "em" = @DI\Inject("doctrine.orm.entity_manager"),
     * })
     */
    public function __construct($em)
    {
        $this->em = $em;
    }

    public function findOrCreate(Language $language, $length, $points)
    {
        $wordLength = $this->em->getRepository(WordLength::class)->findOneBy(['length' => $length, 'points' => $points, 'language' => $language]);
        if (!$wordLength) {
            $wordLength = new WordLength();
            $wordLength->setLanguage($language);
            $wordLength->setPoints($points);
            $wordLength->setLength($length);

            $this->em->persist($wordLength);
            $this->em->flush();
        }

        return $wordLength;
    }
}
