<?php

namespace MagicWordBundle\Manager;

use Doctrine\ORM\EntityManagerInterface;
use JMS\DiExtraBundle\Annotation as DI;
use LexiconBundle\Entity\Language;
use LexiconBundle\Entity\Letter;
use MagicWordBundle\Entity\Rules\ComboPoints;
use MagicWordBundle\Entity\Rules\WordLength;
use MagicWordBundle\Form\Type\ComboPointsType;
use MagicWordBundle\Form\Type\LetterLanguagePointsType;
use MagicWordBundle\Form\Type\WordLengthType;
use Symfony\Component\HttpFoundation\Request;

/**
 * @DI\Service("mw_manager.rules")
 */
class RulesManager
{
    protected $em;
    protected $formFactory;

    /**
     * @DI\InjectParams({
     *      "entityManager" = @DI\Inject("doctrine.orm.entity_manager"),
     *      "formFactory"   = @DI\Inject("form.factory"),
     * })
     */
    public function __construct($entityManager, $formFactory)
    {
        $this->em = $entityManager;
        $this->formFactory = $formFactory;
    }

    public function findOrCreateWordLength(Language $language, $length, $points)
    {
        $wordLength = $this->em->getRepository(WordLength::class)->findOneBy(['length' => $length, 'points' => $points, 'language' => $language]);
        if (!$wordLength) {
            $wordLength = new WordLength();
            $wordLength->setLanguage($language);
            $wordLength->setPoints($points);
            $wordLength->setLength($length);

            $this->em->persist($wordLength);
            $this->em->flush();
        }

        return $wordLength;
    }

    public function findOrCreateCombo(Language $language, $length, $points)
    {
        $combo = $this->em->getRepository(ComboPoints::class)->findOneBy(['length' => $length, 'points' => $points, 'language' => $language]);
        if (!$combo) {
            $combo = new ComboPoints();
            $combo->setLanguage($language);
            $combo->setPoints($points);
            $combo->setLength($length);

            $this->em->persist($combo);
            $this->em->flush();
        }

        return $combo;
    }

    public function getWordLengthForm(WordLength $wordLength)
    {
        $form = $this->formFactory->createBuilder(WordLengthType::class, $wordLength)->getForm()->createView();

        return $form;
    }

    public function getComboPointForm(ComboPoints $comboPoints)
    {
        $form = $this->formFactory->createBuilder(ComboPointsType::class, $comboPoints)->getForm()->createView();

        return $form;
    }

    public function handleWordLengthForm(WordLength $wordlength, Request $request)
    {
        $form = $this->formFactory->createBuilder(WordLengthType::class, $wordlength)->getForm();
        $form->handleRequest($request);

        if ($form->isValid()) {
            $this->em->persist($wordlength);
            $this->em->flush();
        }

        return;
    }

    public function handleComboPointForm(ComboPoints $comboPoints, Request $request)
    {
        $form = $this->formFactory->createBuilder(ComboPointsType::class, $comboPoints)->getForm();
        $form->handleRequest($request);

        if ($form->isValid()) {
            $this->em->persist($comboPoints);
            $this->em->flush();
        }

        return;
    }

    public function getLetterForm(Letter $letter)
    {
        $form = $this->formFactory->createBuilder(LetterLanguagePointsType::class, $letter)->getForm()->createView();

        return $form;
    }

    public function handleLetterLanguageForm(Letter $letter, Request $request)
    {
        $form = $this->formFactory->createBuilder(LetterLanguagePointsType::class, $letter)->getForm();
        $form->handleRequest($request);

        if ($form->isValid()) {
            $this->em->persist($letter);
            $this->em->flush();
        }

        return;
    }
}
