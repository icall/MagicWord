<?php

namespace  MagicWordBundle\Manager;

use JMS\DiExtraBundle\Annotation as DI;
use LexiconBundle\Entity\Language;
use LexiconBundle\Entity\Word;
use LexiconBundle\Entity\WordStart;
use MagicWordBundle\Entity\Grid;
use Symfony\Component\HttpFoundation\Request;

/**
 * @DI\Service("mw_manager.grid")
 */
class GridManager
{
    protected $em;
    protected $squareManager;
    protected $languageManager;
    protected $foundableFormManager;
    protected $tokenStorage;

    /**
     * @DI\InjectParams({
     *      "entityManager"         = @DI\Inject("doctrine.orm.entity_manager"),
     *      "foundableFormManager"  = @DI\Inject("mw_manager.foundableform"),
     *      "squareManager"         = @DI\Inject("mw_manager.square"),
     *      "languageManager"       = @DI\Inject("lexicon_manager.language"),
     *      "tokenStorage"          = @DI\Inject("security.token_storage"),
     * })
     */
    public function __construct($entityManager, $foundableFormManager, $squareManager, $languageManager, $tokenStorage)
    {
        $this->em = $entityManager;
        $this->foundableFormManager = $foundableFormManager;
        $this->squareManager = $squareManager;
        $this->languageManager = $languageManager;
        $this->tokenStorage = $tokenStorage;
    }

    private function newGrid($language, $side)
    {
        $grid = new Grid();
        $grid->setLanguage($language);
        $grid->setSide($side);

        return $grid;
    }


    public function generate(Language $language, $debug = false, $minBigram = null)
    {
        $grid = $this->newGrid($language, 4);

        $minBigram = ($minBigram == null) ? $language->getMinBigram() : $minBigram;
        $letters = $this->lottery($language, 4, $debug, $minBigram);

        if ($debug === true) {
            echo "\nletters: ";
            foreach ($letters as $letter) {
                echo $letter;
            }
            echo "\n";
        }

        foreach ($letters as $letter) {
            $square = $this->squareManager->create($letter, $grid);
            $grid->addSquare($square);
        }
        $words = $this->findWords($grid);
        $this->foundableFormManager->populateFoundables($words, $grid);

        return $grid;
    }


    public function createGrid($request, $save)
    {
        $languageId = $request->request->get('language');
        $language = $this->em->getRepository('LexiconBundle:Language')->find($languageId);

        $grid = $this->newGrid($language, 4);
        $this->addSquares($grid, $request->request->get('squares'));

        if ($save) {
            $grid = $this->saveFoundables($grid);
        }

        return $grid;
    }

    public function updateGrid(Grid $grid, Request $request, $round)
    {
        $grid->setLanguage($round->getLanguage());
        $this->removeSquares($grid);

        $this->removeFoundables($grid);

        $this->em->refresh($grid);

        $this->addSquares($grid, $request->request->get('squares'));
        $grid = $this->saveFoundables($grid);

        $this->em->persist($grid);
        $this->em->flush($grid);

        return $grid;
    }

    public function saveFoundables(Grid $grid)
    {
        $this->em->getConnection()->getConfiguration()->setSQLLogger(null);
        $inflections = $this->findWords($grid);
        $this->foundableFormManager->populateFoundables($inflections, $grid);

        $this->em->persist($grid);
        $this->em->flush();

        return $grid;
    }

    public function countLemmasAndCombos(Grid $grid)
    {
        $words = $this->retrieveInflections($grid);
        $lemmas = $this->getCombos($words);
        $lemmaCount = count($lemmas);

        $combos = array_filter($lemmas, function ($v) {
            return count($v['words']) > 1;
        });
        $average = $this->getComboAverage($combos);

        return ["lemmaCount" => $lemmaCount, "comboAverage" => $average, "comboCount" => count($combos), "lemmas" => $lemmas];
    }

    private function removeSquares(Grid $grid)
    {
        $squares = $grid->getSquares();
        foreach ($squares as $square) {
            $this->em->remove($square);
        }
        $this->em->flush();

        return;
    }

    private function removeFoundables(Grid $grid)
    {
        $foundables = $grid->getFoundableForms();
        foreach ($foundables as $foundable) {
            $this->em->remove($foundable);
        }

        $this->em->flush();
    }

    private function addSquares(Grid $grid, $letters)
    {
        foreach ($letters as $letter) {
            $grid->addSquare($this->squareManager->create(strtolower($letter), $grid));
        }

        return $grid;
    }

    public function getInflections(Request $request)
    {
        $grid = $this->createGrid($request, false);
        $inflections = $this->findWords($grid);

        return $inflections;
    }

    public function getFoundableForms(Request $request)
    {
        $grid = $this->createGrid($request, false);
        $inflections = $this->findWords($grid);

        $this->foundableFormManager->populateFoundables($inflections, $grid);

        return $grid->getFoundableForms();
    }

    public function getComboAverage($combos)
    {
        $length = count($combos);
        $totalCombo = 0;
        foreach ($combos as $combo) {
            $totalCombo += count($combo["words"]);
        }
        $average = $totalCombo/$length;

        return $average;
    }

    public function getCombos($words)
    {
        $combos = [];
        foreach ($words as $word) {
            $root = $word->getRoot();
            $form = $word->getCleanValue();
            $rootId = $root->getId();

            if (!isset($combos[$rootId])) {
                $combos[$rootId] = [
                    'root' => [
                        'id' => $rootId,
                        'value' => $root->getValue(),
                    ],
                    'words' => [],
                ];
            }

            if (!in_array($form, $combos[$rootId]['words'])) {
                $combos[$rootId]['words'][] = $form;
            }
        }

        $lemmas = array_filter($combos, function ($v) {
            return count($v['words']) > 0;
        });

        usort($lemmas, function ($a, $b) {
            return count($b['words']) - count($a['words']);
        });

        return $lemmas;
    }

    public function findWords(Grid $grid)
    {
        $this->em->getConnection()->getConfiguration()->setSQLLogger(null);
        $side = $grid->getSide();
        $i = 0;
        $simplifiedGrid = [];
        $wordStarts = [];
        $this->currentLanguage = $grid->getLanguage();

        // on créé un tableau plus simple à manipuler.
        for ($y = 0; $y < $side; ++$y) {
            for ($x = 0; $x < $side; ++$x) {
                $simplifiedGrid[$y][$x] = ($grid->getSquares()[$i])
                  ? $grid->getSquares()[$i]->getLetter()->getValue()
                  : null;

                ++$i;
            }
        }

        // on l'alimente par récursivité avec la méthode nextLetter() en partant de chaque lettre
        for ($y = 0; $y < $side; ++$y) {
            for ($x = 0; $x < $side; ++$x) {
                // ajout au tableau des mots potentiels de ceux qui commencent par la lettre en x, y
                $wordStarts = array_unique(array_merge($wordStarts, $this->nextLetter('', $simplifiedGrid, $x, $y, $side)), SORT_STRING);
            }
        }


        // words contient tous les débuts de mots ayant été trouvé dans le dictionnaire
        // il faut vérifier si chaque word existe réellement dans le dictionnaire

        if ($wordStarts) {
            return $this->em->getRepository(Word::class)->getExistingWords($wordStarts, $this->currentLanguage);
        }

        return [];
    }


    private function nextLetter($word, $grid, $x, $y, $side)
    {
        $this->em->getConnection()->getConfiguration()->setSQLLogger(null);
        // ajouter la lettre en x, y au mot courant
        $word .= $grid[$y][$x];
        // la détruire dans la grille
        $grid[$y][$x] = null;

        $wordLongEnough = mb_strlen($word) > 1;

        if ($wordLongEnough) {
            // vérifier en bdd s'il existe des mots qui commencent par $word à partir de 2 lettres
            // $startExists = $this->em->getRepository(WordStart::class)->search($word, $this->currentLanguage->getId());
            $startExists = $this->em->getRepository(WordStart::class)->findOneBy([
                "value" => $word,
                "language" => $this->currentLanguage
            ]);
            // si pas de mot dans le dico commençant par le mot en cours, ne pas retourner le mot et arrêter la recherche
            if (!$startExists) {
                return [];
            }
        }
        // stocker le début de mot dès qu'il atteint 2 lettres
        $words = $wordLongEnough ? array($word) : [];

        //
        // On calcule la position de la lettre suivante (autour de la lettre en cours)
        //
        // ligne du dessus
        $yy = $y - 1;
        if ($yy >= 0) {
            $xx = $x - 1;
            // on vérifie que la lettre n'a pas été utilisée (donc détruite)
            if (($xx >= 0) && ($grid[$yy][$xx] != null)) {
                // appel en récursif pour ajout d'une nouvelle lettre au mot
                $words = array_merge($words, $this->nextLetter($word, $grid, $xx, $yy, $side));
            }

            $xx = $x;
            if ($grid[$yy][$xx] != null) {
                $words = array_merge($words, $this->nextLetter($word, $grid, $xx, $yy, $side));
            }

            $xx = $x + 1;
            if (($xx < $side) && ($grid[$yy][$xx] != null)) {
                $words = array_merge($words, $this->nextLetter($word, $grid, $xx, $yy, $side));
            }
        }

        // ligne courante
        $yy = $y;
        $xx = $x - 1;
        if (($xx >= 0) && ($grid[$yy][$xx] != null)) {
            $words = array_merge($words, $this->nextLetter($word, $grid, $xx, $yy, $side));
        }
        $xx = $x + 1;
        if (($xx < $side) && ($grid[$yy][$xx] != null)) {
            $words = array_merge($words, $this->nextLetter($word, $grid, $xx, $yy, $side));
        }

        // ligne du dessous
        $yy = $y + 1;
        if ($yy < $side) {
            $xx = $x - 1;
            if (($xx >= 0) && ($grid[$yy][$xx] != null)) {
                $words = array_merge($words, $this->nextLetter($word, $grid, $xx, $yy, $side));
            }

            $xx = $x;
            if ($grid[$yy][$xx] != null) {
                $words = array_merge($words, $this->nextLetter($word, $grid, $xx, $yy, $side));
            }

            $xx = $x + 1;
            if (($xx < $side) && ($grid[$yy][$xx] != null)) {
                $words = array_merge($words, $this->nextLetter($word, $grid, $xx, $yy, $side));
            }
        }

        // renvoyer les débuts de mot trouvés
        return $words;
    }


    public function retrieveInflections(Grid $grid)
    {
        $words = [];
        $foundables = $grid->getFoundableForms();
        foreach ($foundables as $foundable) {
            foreach ($foundable->getWords() as $word) {
                $words[] = $word;
            }
        }

        return $words;
    }

    public function export(Grid $grid = null)
    {
        if ($grid) {
            $gridJSON = [
                'language' => $grid->getLanguage()->getId(),
                'letters' => [],
            ];

            foreach ($grid->getSquares() as $square) {
                $gridJSON['letters'][] = $square->getLetter()->getValue();
            }

            return $gridJSON;
        }

        return;
    }

    public function lottery(Language $language, $side, $debug = false, $minBigram = null, $grid = null)
    {
        $minBigram = ($minBigram == null) ? $language->getMinBigram() : $minBigram;
        // On crée un tableau de bigrammes
        // où chaque bigramme apparait autant de fois que son poids
        // Modif: on  garde les $minBigram pourcent les plus productifs des bigrammes
        // Génération d'un tableau qui a chaque bigram associe le % de bigram moins fréquents (centile)
        $centilesBigrams = [];
        $poidsBigrams = [];
        $bigramsToFreq = [];
        $nbBigrams=0;
        $lines = $this->languageManager->getBigrams($language);
        array_shift($lines);
        foreach ($lines as $line) {
            $tab = explode("\t", $line);
            $string = $tab[0];
            $freqBigramBi = $tab[1];
            $poids = $tab[3];
            $nbBigrams++;
            $bigramsToFreq[$string]=$freqBigramBi;
            $poidsBigrams[$string]=$freqBigramBi;
        }
        asort($bigramsToFreq);
        $nbInferiorBigram=0;
        $bigrams = [];
        // $lines = $this->languageManager->getBigrams($language);
        $lettersFromBigrams=[];
        // array_shift($lines);
        foreach ($bigramsToFreq as $bigramToFreq => $value) {
            $nbInferiorBigram++;
            $centile = 100 - intval(($nbBigrams-$nbInferiorBigram)*100/$nbBigrams);
            $centilesBigrams[$bigramToFreq]=$centile;
            if ($centilesBigrams[$bigramToFreq] >= $minBigram) {
                $bigramLetters = preg_split('//u', $bigramToFreq, null, PREG_SPLIT_NO_EMPTY);
                foreach ($bigramLetters as $bigramLetter) {
                    $lettersFromBigrams[$bigramLetter] = 1;
                }

                for ($j = 0; $j < $poidsBigrams[$bigramToFreq]; $j++) {
                    $bigrams[] = $bigramToFreq;
                }
            }
        }

        $countsBigram = array_count_values($bigrams);

        // // On initialise une grille vide.
        if (!$grid) {
            $grid = [];
            for ($i=0; $i < $side; $i++) {
                for ($j=0; $j < $side; $j++) {
                    $grid[$i][$j] = null;
                }
            }
        }

        $emptySquares = $this->getEmptySquares($grid, $side);
        //tant qu'il reste des cases vides
        while (!empty($emptySquares)) {
            $bigram = $bigrams[array_rand($bigrams)];
            $emptySquare = $emptySquares[array_rand($emptySquares)];
            $emptySquareI = $emptySquare[0];
            $emptySquareJ = $emptySquare[1];

            $chars = preg_split('//u', $bigram, -1, PREG_SPLIT_NO_EMPTY);
            $firstLetter = $chars[0];
            $secondLetter = $chars[1];
            $flagBigram = false;
            // on regarde si une lettre du bigram pioché est déjà présente et qu'elle a des adjacent vide.
            for ($i=0; $i < $side; $i++) {
                for ($j=0; $j < $side; $j++) {
                    if ($grid[$i][$j] == $firstLetter) {
                        $ajdacentsLetterBigram = $this->getAdjacentSquares($grid, $side, [$i, $j], true);
                        if (!empty($ajdacentsLetterBigram)) {
                            $ajdacentLetterBigram = $ajdacentsLetterBigram[array_rand($ajdacentsLetterBigram)];
                            $grid[$ajdacentLetterBigram[0]][$ajdacentLetterBigram[1]] = $secondLetter;
                            $flagBigram = true;
                            break 2;
                        }
                    } elseif ($grid[$i][$j] == $secondLetter) {
                        $ajdacentsLetterBigram = $this->getAdjacentSquares($grid, $side, [$i, $j], true);
                        if (!empty($ajdacentsLetterBigram)) {
                            $ajdacentLetterBigram = $ajdacentsLetterBigram[array_rand($ajdacentsLetterBigram)];
                            $grid[$ajdacentLetterBigram[0]][$ajdacentLetterBigram[1]] = $firstLetter;
                            $flagBigram = true;
                            break 2;
                        }
                    }
                }
            }
            if (!$flagBigram) {
                $ajdacentEmptySquares = $this->getAdjacentSquares($grid, $side, $emptySquare, true);

                if (empty($ajdacentEmptySquares)) {
                    $ajdacentSquares = $this->getAdjacentSquares($grid, $side, $emptySquare, false);
                    $letterToScore=[];
                    foreach ($lettersFromBigrams as $key => $value) {
                        $score=0;
                        foreach ($ajdacentSquares as $ajdacentSquare) {
                            $letter = $grid[$ajdacentSquare[0]][$ajdacentSquare[1]];
                            if (isset($countsBigram[$letter.$key])) {
                                $score += $countsBigram[$letter.$key];
                            } elseif (isset($countsBigram[$key.$letter])) {
                                $score += $countsBigram[$key.$letter];
                            }
                        }
                        $letterToScore[$key] = $score;
                    }
                    arsort($letterToScore);
                    reset($letterToScore);
                    $bestLetter = key($letterToScore);
                    $grid[$emptySquareI][$emptySquareJ] = $bestLetter;
                } else {
                    $randomAdjacent = $ajdacentEmptySquares[array_rand($ajdacentEmptySquares)];
                    $grid[$emptySquareI][$emptySquareJ] = $firstLetter;
                    $grid[$randomAdjacent[0]][$randomAdjacent[1]] = $secondLetter;
                }
            }
            $emptySquares = $this->getEmptySquares($grid, $side);
        }

        $letters = [];
        for ($i=0; $i < $side; $i++) {
            for ($j=0; $j < $side; $j++) {
                $letters[] = $grid[$i][$j];
            }
        }
        return $letters;
    }

    private function getAdjacentSquares($grid, $side, $square, $returnEmpty = true)
    {
        $emptyAdjacentSquares = [];
        $i = $square[0];
        $j = $square[1];

        $left = $i - 1;
        $right = $i + 1;
        $top = $j - 1;
        $bottom = $j + 1;

        $lookLeft = ($left >= 0) ? true : false;
        $lookTop = ($top >= 0) ? true : false;
        $lookRight = ($right < $side) ? true : false;
        $lookBottom = ($bottom < $side) ? true : false;

        if ($lookTop) {
            if ($lookLeft && $grid[$top][$left] === null) {
                $emptyAdjacentSquares[] = [$top, $left];
            } elseif ($lookLeft) {
                $adjacentSquares[] = [$top, $left];
            }


            if ($grid[$top][$j] === null) {
                $emptyAdjacentSquares[] = [$top, $j];
            }
            $adjacentSquares[] = [$top, $j];
            if ($lookRight && $grid[$top][$right] === null) {
                $emptyAdjacentSquares[] = [$top, $right];
            } elseif ($lookRight) {
                $adjacentSquares[] = [$top, $right];
            }
        }

        if ($lookRight && $grid[$i][$right] === null) {
            $emptyAdjacentSquares[] = [$i, $right];
        } elseif ($lookRight) {
            $adjacentSquares[] = [$i, $right];
        }


        if ($lookBottom) {
            if ($lookRight && $grid[$bottom][$right] === null) {
                $emptyAdjacentSquares[] = [$bottom, $right];
            } elseif ($lookRight) {
                $adjacentSquares[] = [$bottom, $right];
            }
            $adjacentSquares[] = [$bottom, $j];
            if ($grid[$bottom][$j] === null) {
                $emptyAdjacentSquares[] = [$bottom, $j];
            }
            if ($lookLeft && $grid[$bottom][$left] === null) {
                $emptyAdjacentSquares[] = [$bottom, $left];
            } elseif ($lookLeft) {
                $adjacentSquares[] = [$bottom, $left];
            }
        }

        if ($lookLeft && $grid[$i][$left] === null) {
            $emptyAdjacentSquares[] = [$i, $left];
        } elseif ($lookLeft) {
            $adjacentSquares[] = [$i, $left];
        }


        return ($returnEmpty) ? $emptyAdjacentSquares : $adjacentSquares;
    }

    private function getEmptySquares($grid, $side)
    {
        $emptySquares = [];

        for ($i=0; $i < $side; $i++) {
            for ($j=0; $j < $side; $j++) {
                if ($grid[$i][$j] === null) {
                    $emptySquares[] = [$i, $j];
                }
            }
        }

        return $emptySquares;
    }
}
