<?php

namespace MagicWordBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Index;

/**
 * @ORM\Table(name="foundable_form", indexes={
 *  @Index(columns={"form"}, name="form"),
 * }))
 * @ORM\Entity(repositoryClass="MagicWordBundle\Repository\FoundableRepository")
 */
class FoundableForm implements \JsonSerializable
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="form", type="string", length=16)
     */
    private $form;

    /**
     * @var int
     *
     * @ORM\Column(name="points", type="integer")
     */
    private $points;

    /**
     * @ORM\ManyToMany(targetEntity="LexiconBundle\Entity\Word")
     * @ORM\JoinTable(name="words_foundable")
     */
    private $words;

    /**
     * @ORM\ManyToOne(targetEntity="Grid", inversedBy="foundableForms")
     */
    private $grid;
    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->inflections = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set form.
     *
     * @param string $form
     *
     * @return FoundableForm
     */
    public function setForm($form)
    {
        $this->form = $form;

        return $this;
    }

    /**
     * Get form.
     *
     * @return string
     */
    public function getForm()
    {
        return $this->form;
    }

    /**
     * Set points.
     *
     * @param int $points
     *
     * @return FoundableForm
     */
    public function setPoints($points)
    {
        $this->points = $points;

        return $this;
    }

    /**
     * Get points.
     *
     * @return int
     */
    public function getPoints()
    {
        return $this->points;
    }

    /**
     * Set grid.
     *
     * @param \MagicWordBundle\Entity\Grid $grid
     *
     * @return FoundableForm
     */
    public function setGrid(\MagicWordBundle\Entity\Grid $grid = null)
    {
        $this->grid = $grid;

        return $this;
    }

    /**
     * Get grid.
     *
     * @return \MagicWordBundle\Entity\Grid
     */
    public function getGrid()
    {
        return $this->grid;
    }

    public function jsonSerialize()
    {
        $jsonArray = array(
            'id' => $this->id,
            'form' => $this->form,
        );

        return $jsonArray;
    }

    /**
     * Add word.
     *
     * @param \LexiconBundle\Entity\Word $word
     *
     * @return FoundableForm
     */
    public function addWord(\LexiconBundle\Entity\Word $word)
    {
        $this->words[] = $word;

        return $this;
    }

    /**
     * Remove word.
     *
     * @param \LexiconBundle\Entity\Word $word
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeWord(\LexiconBundle\Entity\Word $word)
    {
        return $this->words->removeElement($word);
    }

    /**
     * Get words.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getWords()
    {
        return $this->words;
    }
}
