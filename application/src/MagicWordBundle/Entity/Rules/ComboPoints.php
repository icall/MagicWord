<?php

namespace MagicWordBundle\Entity\Rules;

use LexiconBundle\Entity\Language;
use Doctrine\ORM\Mapping as ORM;

/**
 * ComboPoints.
 *
 * @ORM\Table(name="combo_points")
 * @ORM\Entity()
 */
class ComboPoints
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="length", type="integer")
     */
    private $length;

    /**
     * @var int
     *
     * @ORM\Column(name="points", type="integer")
     */
    private $points;

    /**
     * @ORM\ManyToOne(targetEntity="LexiconBundle\Entity\Language", inversedBy="comboPoints")
     * @ORM\JoinColumn(nullable=false)
     */
    private $language;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set length.
     *
     * @param int $length
     *
     * @return WordLengthPoints
     */
    public function setLength($length)
    {
        $this->length = $length;

        return $this;
    }

    /**
     * Get length.
     *
     * @return int
     */
    public function getLength()
    {
        return $this->length;
    }

    /**
     * Set points.
     *
     * @param int $points
     *
     * @return WordLengthPoints
     */
    public function setPoints($points)
    {
        $this->points = $points;

        return $this;
    }

    /**
     * Get points.
     *
     * @return int
     */
    public function getPoints()
    {
        return $this->points;
    }

    public function getLanguage(): ?Language
    {
        return $this->language;
    }

    public function setLanguage(?Language $language): self
    {
        $this->language = $language;

        return $this;
    }
}
