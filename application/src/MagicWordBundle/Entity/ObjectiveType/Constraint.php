<?php

namespace MagicWordBundle\Entity\ObjectiveType;

use Doctrine\ORM\Mapping as ORM;
use MagicWordBundle\Entity\Objective;

/**
 * Constraint.
 *
 * @ORM\Entity
 * @ORM\Table(name="objective_type_constraint")
 */
class Constraint extends Objective
{
    protected $discr = 'constraint';

    public function getDiscr()
    {
        return $this->discr;
    }

    /**
     * @var int
     *
     * @ORM\Column(name="numberToFind", type="integer")
     */
    private $numberToFind;

    /**
     * @ORM\ManyTomany(targetEntity="LexiconBundle\Entity\Feature")
     */
    private $features;

    /**
     * Set numberToFind.
     *
     * @param int $numberToFind
     *
     * @return Constraint
     */
    public function setNumberToFind($numberToFind)
    {
        $this->numberToFind = $numberToFind;

        return $this;
    }

    /**
     * Get numberToFind.
     *
     * @return int
     */
    public function getNumberToFind()
    {
        return $this->numberToFind;
    }



    public function export()
    {
        $jsonArray = array(
            'type' => $this->discr,
            'numberToFind' => $this->numberToFind,
            'features' => []
        );

        foreach ($this->features as $feature) {
            $jsonArray["features"][] = $feature->getId();
        }

        return $jsonArray;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->features = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add feature.
     *
     * @param \LexiconBundle\Entity\Feature $feature
     *
     * @return Constraint
     */
    public function addFeature(\LexiconBundle\Entity\Feature $feature)
    {
        $this->features[] = $feature;

        return $this;
    }

    /**
     * Remove feature.
     *
     * @param \LexiconBundle\Entity\Feature $feature
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeFeature(\LexiconBundle\Entity\Feature $feature)
    {
        return $this->features->removeElement($feature);
    }

    /**
     * Get features.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getFeatures()
    {
        return $this->features;
    }
}
